var config = {
    map: {
        '*': {
            // TODO: Use camelCase for module names
            'dba-trigger': 'Swissclinic_DBA/js/dba-trigger',
            'add-to-cart-form': 'Swissclinic_DBA/js/add-to-cart-form',
        },
    },
    shim: {
        'dba-trigger': ['jquery'],
    },
};

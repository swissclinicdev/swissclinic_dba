define([
    "jquery",
], function($){
    "use strict";
    return function(config, element) {
        $('#dba-modal').modal({
            buttons: [],
            modalClass: 'dba-modal',
            autoOpen: true, // TODO: Remove.
        });

        $(element).click(function() {
            $('#dba-modal').modal('openModal');
        });
    };
});

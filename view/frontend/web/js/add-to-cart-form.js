define([
    "jquery",
    "catalogAddToCart",
], function($){
    "use strict";
    return function(config, element) {
        $(element).catalogAddToCart(config);
    };
});

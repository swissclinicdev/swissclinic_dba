define([
    'jquery',
    'uiComponent',
    'ko',
    'mage/storage',
], function ($, Component, ko, storage) {
    'use strict';

    // --- GLOBAL FUNCTIONS ---
    function log(str) {
        console.log('[dba] ' + str);
    }

    function getFromLocalStorage(key) {
        var json = localStorage.getItem('dba_' + key);

        if (json === null) {
            return null;
        }

        log('got ' + key + '=' + json + ' from localStorage');
        return JSON.parse(json);
    }

    function storeInLocalStorage(key, value) {
        var json = JSON.stringify(value);
        localStorage.setItem('dba_' + key, json);
        log('stored ' + key + '=' + json + ' in localStorage');
    }

    function logGAEvent(action, value) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Tests',
            eventLabel: 'Digital Beauty Advisor',
            eventAction: action,
            eventValue: value,
        });
        log('logged event (' + action + ', ' + value + ') to GA');
    }

    function getTest() {
        log('fetching test...');
        return storage.get('/dba/test').then(function (test) {
            log('got test=' + JSON.stringify(test));
            return test;
        });
    }

    function getResult(choiceIds) {
        log('fetching result... choiceIds: ' + choiceIds);
        return storage.get('/dba/result?choiceIds=' + choiceIds.join(',')).then(function (result) {
            log('got result=' + JSON.stringify(result));
            return result;
        });
    }

    function createSession() {
        log('creating session...');
        return storage.post(
            '/dba/session/create',
        ).then(function (dbaSessionId) {
            log('created session with id=' + dbaSessionId);
            return dbaSessionId;
        });
    }

    function completeSession(dbaSessionId, choiceIds) {
        log('completing session...');
        return storage.post(
            '/dba/session/complete',
            JSON.stringify({
                dbaSessionId: dbaSessionId,
                choiceIds: choiceIds,
            }),
        ).then(function () {
            log('completed session!');
        });
    }

    return Component.extend({
        // --- INITIALIZATION ---
        initialize: function () {
            this._super();

            this.test = ko.observable(null);

            this.activeQuestionIndex = ko.observable(-1);
            this.answers = [];

            this.fetchingResult = ko.observable(false);
            this.result = ko.observable(null);

            this.dbaSessionId = null;

            var loaded = this.loadDataFromLocalStorage();
            if (!loaded) {
                this.fetchTest();
            }
        },
        setModalClass: function () {
            var classes = [
                'fetching-test',
                'not-started',
                'in-progress',
                'fetching-result',
                'finished',
            ];
            var classMap = {
                [this.states.fetchingTest]: 'fetching-test',
                [this.states.notStarted]: 'not-started',
                [this.states.inProgress]: 'in-progress',
                [this.states.fetchingResult]: 'fetching-result',
                [this.states.finished]: 'finished',
            };

            var modalWrapper = $('.dba-modal .modal-inner-wrap');
            classes.forEach(modalWrapper.removeClass.bind(modalWrapper));
            modalWrapper.addClass(classMap[this.getState()]);
        },
        loadDataFromLocalStorage: function () {
            var test = getFromLocalStorage('test');
            var activeQuestionIndex = getFromLocalStorage('activeQuestionIndex');
            var answers = getFromLocalStorage('answers');
            var result = getFromLocalStorage('result');
            var dbaSessionId = getFromLocalStorage('dbaSessionId');

            if (test === null) {
                log('no localStorage found');
                return false;
            }

            this.test(test);
            this.activeQuestionIndex(activeQuestionIndex === null ? -1 : activeQuestionIndex);
            this.answers = answers === null ? [] : answers;
            // TODO: Handle expired form-keys in result
            this.result(result);
            this.dbaSessionId = dbaSessionId;

            log('loaded data from localStorage');

            return true;
        },
        fetchTest: function () {
            getTest().then((function (test) {
                this.setTest(test);
            }).bind(this)).fail(function (err) {
                console.error(err);
            });
        },

        // --- ACTIONS ---
        startTest: function () {
            if (this.test) {
                log('started test');
                this.showNextQuestion();
                if (this.dbaSessionId === null) {
                    // If the user starts the test again before the session has been created,
                    // we don't want to create a new session.
                    // dbaSessionId is set to -1 to indicate that a session is being created,
                    // so that we don't get any race conditions here.
                    this.dbaSessionId = -1;
                    createSession().then((function (dbaSessionId) {
                        this.setDbaSessionId(dbaSessionId);
                        logGAEvent('Started', dbaSessionId);
                    }).bind(this));
                }
            } else {
                log('test not fetched');
            }
        },
        selectChoice: function (choice) {
            log('selected choice ' + choice.id + ': ' + choice.title);
            this.setAnswerToCurrentQuestion(choice.id);
            this.showNextQuestion();
        },
        goToPreviousQuestion: function () {
            this.setActiveQuestionIndex(this.activeQuestionIndex() - 1);
            if (this.activeQuestionIndex() === -1) {
                log('showing start screen')
            } else {
                log('showing question ' + this.getActiveQuestion().id + ': ' + this.getActiveQuestion().title);
            }
        },
        addAllToCart: function () {
            $('#dba-modal-content button.tocart').click();
            log('added all products to cart');
        },
        restartTest: function () {
            this.setActiveQuestionIndex(-1);
            this.setResult(null);
            this.clearAnswers();
            this.setDbaSessionId(null);
            log('restarted test');
        },
        closeModal: function () {
            $('#dba-modal').modal('closeModal');
        },

        // --- VIEW HELPERS ---
        showNextQuestion: function () {
            if (this.isOnLastQuestion()) {
                log('reached last question. answers: ' + this.answers);
                this.setActiveQuestionIndex(-1);
                this.finishTest();
            } else {
                this.setActiveQuestionIndex(this.activeQuestionIndex() + 1);
                if (this.test()) {
                    log('showing question ' + this.getActiveQuestion().id + ': ' + this.getActiveQuestion().title);
                } else {
                    log('still fetching test')
                }
            }
        },
        isOnFirstQuestion: function () {
            return this.activeQuestionIndex() === 0;
        },
        isOnLastQuestion: function () {
            return this.test() && this.activeQuestionIndex() === this.test().questions.length - 1;
        },
        isSelectedChoice: function (choice) {
            return this.answers.includes(choice.id);
        },
        finishTest: function () {
            this.fetchingResult(true);
            getResult(this.answers).then((function (result) {
                this.setResult(result);
                this.fetchingResult(false);
            }).bind(this)).fail(function (err) {
                console.error(err);
            });

            completeSession(
                this.dbaSessionId,
                this.answers
            );
        },

        // --- GETTERS & SETTERS ---
        setTest: function (test) {
            this.test(test);
            storeInLocalStorage('test', test);
        },
        setDbaSessionId: function (dbaSessionId) {
            this.dbaSessionId = dbaSessionId;
            storeInLocalStorage('dbaSessionId', dbaSessionId);
        },
        setActiveQuestionIndex: function (activeQuestionIndex) {
            this.activeQuestionIndex(activeQuestionIndex);
            storeInLocalStorage('activeQuestionIndex', activeQuestionIndex);
        },
        getActiveQuestion: function () {
            var activeQuestionIndex = this.activeQuestionIndex();
            return activeQuestionIndex === -1 ? null : this.test().questions[activeQuestionIndex];
        },
        getActiveQuestionNumber: function () {
            return this.activeQuestionIndex() + 1;
        },
        getTotalNumberOfQuestions: function () {
            return this.test().questions.length;
        },
        setAnswerToCurrentQuestion: function (answer) {
            this.answers[this.activeQuestionIndex()] = answer;
            storeInLocalStorage('answers', this.answers);
        },
        clearAnswers: function () {
            this.answers = [];
            storeInLocalStorage('answers', []);
        },
        setResult: function (result) {
            this.result(result);
            storeInLocalStorage('result', result);
        },

        // --- STATE ---
        states: Object.freeze({
            fetchingTest: 1,
            notStarted: 2,
            inProgress: 3,
            fetchingResult: 4,
            finished: 5,
        }),
        getState: function () {
            if (this.fetchingResult()) {
                return this.states.fetchingResult;
            } else if (this.result() !== null) {
                return this.states.finished;
            } else if (this.activeQuestionIndex() === -1) {
                return this.states.notStarted;
            } else if (this.test() === null) {
                return this.states.fetchingTest;
            } else {
                return this.states.inProgress;
            }
        },
        getStateClass: function () {
            switch (this.getState()) {
                case this.states.fetchingTest:
                    return 'fetching-test';
                case this.states.notStarted:
                    return 'not-started';
                case this.states.inProgress:
                    return 'in-progress';
                case this.states.fetchingResult:
                    return 'fetching-result';
                case this.states.finished:
                    return 'finished';
                default:
                    return '';
            }
        },
        isFetchingTest: function () {
            return this.getState() === this.states.fetchingTest;
        },
        isNotStarted: function () {
            return this.getState() === this.states.notStarted;
        },
        isInProgress: function () {
            return this.getState() === this.states.inProgress;
        },
        isFetchingResult: function () {
            return this.getState() === this.states.fetchingResult;
        },
        isFinished: function () {
            return this.getState() === this.states.finished;
        },
    });
});

<?php

namespace Swissclinic\DBA\Model;

class API
{
    private $apiUrl = 'https://dba.swissclinic.com';

    private function get($endpoint, $parameters) {
        $queryStr = http_build_query($parameters);
        $url = "$this->apiUrl$endpoint?$queryStr";
        $res = file_get_contents($url);
        return json_decode($res, true);
    }

    private function post($endpoint, $parameters) {
        $url = $this->apiUrl . $endpoint;
        $options = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => "Content-Type: application/json\r\n",
                'content' => json_encode($parameters),
            )
        );
        $context = stream_context_create($options);
        // TODO: Handle errors in a better way so that the response can be seen
        // during development.
        $res = file_get_contents($url, false, $context);
        return json_decode($res, true);
    }

    public function getTest($language) {
        return $this->get(
            '/test',
            ['language' => $language]
        )['test'];
    }

    public function getResult($language, $choiceIds) {
        return $this->get(
            '/result',
            [
                'language' => $language,
                'choiceIds' => implode(',', $choiceIds),
            ]
        )['result'];
    }

    public function createSession() {
        return $this->post('/create_session', [])['sessionId'];
    }

    public function completeSession($sessionId, $choiceIds) {
        $this->post(
            '/complete_session',
            [
                'sessionId' => $sessionId,
                'choiceIds' => $choiceIds,
            ]
        );
    }
}

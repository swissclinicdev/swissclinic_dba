<?php

namespace Swissclinic\DBA\Controller\Result;

class Index extends \Magento\Framework\App\Action\Action
{
    private $request;
    private $resultJsonFactory;
    private $productRepository;
    private $productHelper;
    private $productViewBlock;
    private $formKey;
    private $productAttributeRepository;
    private $pricingHelper;
    private $imageHelper;
    private $api;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Element\FormKey $formKey,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $productAttributeRepository,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Block\Product\View $productViewBlock,
        \Swissclinic\DBA\Model\API $api)
    {
        parent::__construct($context);
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->productHelper = $productHelper;
        $this->productViewBlock = $productViewBlock;
        $this->formKey = $formKey;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->pricingHelper = $pricingHelper;
        $this->imageHelper = $imageHelper;
        $this->api = $api;
    }

    public function execute()
    {
        // TODO (v1):
        //  - Don't hard code language, figure it out from the request or context
        //  - Implement email functionality
        //  - Add CSS (mobile + desktop)

        // TODO (v2):
        //  - Add extra features (fallback products, multi-choice answers)

        $choiceIds = array_map(
            'intval',
            explode(',', $this->request->getParam('choiceIds'))
        );

        $result = $this->api->getResult('EN', $choiceIds);

        $skus = $this->extractSkusFromResult($result);
        $productsMap = array_reduce($skus, function($map, $sku) {
            $map[$sku] = $this->getProductInfo($sku);
            return $map;
        }, []);
        $response = $this->buildResponse($result, $productsMap);

        return $this->resultJsonFactory->create()->setData($response);
    }

    private function extractSkusFromResult($result) {
        return array_merge(
            [
                $result['treatment']['productId'],
                $result['treatment']['addonProductId']
            ],
            array_map(function($product) {
                return $product['productId'];
            }, $result['extraProducts']['products'])
        );
    }

    private function getProductInfo($sku) {
        $explodedSkus = explode('-', $sku);

        if (count($explodedSkus) == 1) {
            return $this->getSimpleProductInfo($sku);
        } else if (count($explodedSkus) == 2) {
            $configurableProductSku = $explodedSkus[0];
            $simpleProductSku = $sku;
            return $this->getConfigurableProductInfo($configurableProductSku, $simpleProductSku);
        } else {
            throw new Exception('Invalid SKU: ' . $sku);
        }
    }

    private function getSimpleProductInfo($sku) {
        $product = $this->productRepository->get($sku);

        return [
            'name' => $product->getName(),
            'price' => $this->pricingHelper->currency($product->getPrice(), true, false),
            'url' => $product->getProductUrl(),
            'imageUrl' => $this->imageHelper->init($product, 'product_base_image')->getUrl(),
            'addToCart' => [
                'sku' => $sku,
                'action' => $this->productViewBlock->getSubmitUrl($product),
                'enctype' => null,
                'product' => $product->getId(),
                'formKey' => $this->formKey->toHtml()
            ],
            // TODO: Return star ratings and "Customer Favourite/New"
        ];
    }

    private function getConfigurableProductInfo($configurableProductSku, $simpleProductSku) {
        $configurableProduct = $this->productRepository->get($configurableProductSku);
        $simpleProduct = $this->productRepository->get($simpleProductSku);

        $needleLengthAttributeId = $this->productAttributeRepository->get('needle_length')->getAttributeId();
        $needleLengthAttributeValue = $simpleProduct->getCustomAttribute('needle_length')->getValue();

        return [
            'name' => $simpleProduct->getName(),
            'price' => $this->pricingHelper->currency($simpleProduct->getPrice(), true, false),
            'url' => $configurableProduct->getProductUrl() . "#$needleLengthAttributeId=$needleLengthAttributeValue",
            'imageUrl' => $this->imageHelper->init($simpleProduct, 'product_base_image')->getUrl(),
            'addToCart' => [
                'sku' => $configurableProductSku,
                'action' => $this->productViewBlock->getSubmitUrl($configurableProduct),
                'enctype' => 'multipart/form-data',
                'product' => $configurableProduct->getId(),
                'selectedConfigurableOption' => $simpleProduct->getId(),
                'superAttributeId' => $needleLengthAttributeId,
                'superAttributeValue' => $needleLengthAttributeValue,
                'formKey' => $this->formKey->toHtml()
            ],
            // TODO: Return star ratings and "Customer Favourite/New"
        ];
    }

    private function buildResponse($result, $productsMap) {
        $response = $result;

        $response['treatment']['product'] = $productsMap[$result['treatment']['productId']];
        unset($response['treatment']['productId']);

        $response['treatment']['addonProduct'] = $productsMap[$result['treatment']['addonProductId']];
        unset($response['treatment']['addonProductId']);

        $response['extraProducts']['products'] = array_map(function($product) use($productsMap) {
            $sku = $product['productId'];
            return $productsMap[$sku];
        }, $result['extraProducts']['products']);

        return $response;
    }
}

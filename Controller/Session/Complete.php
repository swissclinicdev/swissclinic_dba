<?php

namespace Swissclinic\DBA\Controller\Session;

class Complete extends \Magento\Framework\App\Action\Action
{
    private $request;
    private $resultJsonFactory;
    private $api;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Swissclinic\DBA\Model\API $api)
    {
        parent::__construct($context);
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->api = $api;
    }

    public function execute()
    {
        $body = json_decode($this->request->getContent(), true);
        $dbaSessionId = $body['dbaSessionId'];
        $choiceIds = $body['choiceIds'];
        $this->api->completeSession($dbaSessionId, $choiceIds);
        return $this->resultJsonFactory->create()->setData(['message' => 'session completed']);
    }
}

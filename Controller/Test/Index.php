<?php

namespace Swissclinic\DBA\Controller\Test;

class Index extends \Magento\Framework\App\Action\Action
{
    private $resultJsonFactory;
    private $productRepository;
    private $api;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Swissclinic\DBA\Model\API $api)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->api = $api;
    }

    public function execute()
    {
        $test = $this->api->getTest('EN');
        return $this->resultJsonFactory->create()->setData($test);
    }
}
